# PPM challenge
This proyect is doing with docker, Mysql in the bd, Yii2 framework [PHP] for the API and Backend and Angular 10 for the frontend.

How to check it ??

In the local environment is necesary create the productions files for Angular
```sh
$ npm i
$ npm start
```

Get up the container:
```sh
$ docker-compose up -d
```

Check for the changes in backend/api:
```sh
$ make docker-up
```
And check the urls for:
- "API:      http://localhost:1111/"
- "Backend:  http://localhost:1112/"
- "Fronted:  http://localhost:1113/" use http://localhost:4200 to default URL in develop.
	
Postman Documentation
https://www.getpostman.com/collections/3e4b5cf32c6e932e003c


Next:
Sorry, in the next realease I want to add a builder node container for angular build proccess. 
And is neccesary disabled CORS in Nginx for test in prod angular
