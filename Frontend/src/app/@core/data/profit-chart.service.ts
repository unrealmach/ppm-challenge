import {Injectable} from '@angular/core';
import {NGXLogger} from 'ngx-logger';

export class ProfitChart {
  chartLabel: string[];
  data: number[][];
}

export class ProfitChartBsnode {
  chartLabel: Date[];
  data: number[][];
  title: string[];
}

@Injectable()
export class ProfitChartService {

  private year = [
    '2012',
    '2013',
    '2014',
    '2015',
    '2016',
    '2017',
    '2018',
  ];

  private data = {};

  // private feeder: Feeder;

  constructor(private logger: NGXLogger,
  ) {
    this.data = {
      week: this.getDataForWeekPeriod(),
      month: this.getDataForMonthPeriod(), // <------------
      year: this.getDataForYearPeriod(),
    };
    this.logger.log('constructor ProfitChartService data: ', this.data);

  }

  private getDataForWeekPeriod(): ProfitChart {
   return null;
  }

  private getDataForMonthPeriod(): ProfitChart { // <------------
  return null;
  }

  private getDataForYearPeriod(): ProfitChart {
    const nPoint = this.year.length;

    return {
      chartLabel: this.year,
      data: [
        this.getRandomData(nPoint),
        this.getRandomData(nPoint),
        this.getRandomData(nPoint),
      ],
    };
  }

  private getRandomData(nPoints: number): number[] {
    return Array.from(Array(nPoints)).map(() => {
      return Math.round(Math.random() * 60);
    });
  }

  private getRandomData2(nPoints: number): number[] {
    return Array.from(Array(nPoints)).map(() => {
      return Math.round(Math.random() * 20);
    });
  }

  getProfitChartData(period: string): ProfitChart {
    // this.feederService.getFeeder(period)
    return this.data[period];
  }
}
