export class User {
  id: string;
  name: String;
  email: String;
  password: String;
  gender: String;
  role: String;
  terms: boolean;
  access: Access;

  // createdAt?: Date;
  // updatedAt?: Date;

  // constructor(data?: any) {
  //   // to cast Date type
  //   Object.assign(this, data);
  //   this.createdAt = new Date(data.createdAt);
  //   this.updatedAt = new Date(data.updatedAt);
  // }
}
export class Access {
  companies: string[];
  farms: string[];
  ponds: string[];
}
