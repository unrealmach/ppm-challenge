import {of as observableOf, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NbAuthService} from '@nebular/auth';
import {environment} from '../../../environments/environment';
import {User} from './user';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {CustomErrorHandlerService} from '../utils/custom-error-handler.service';
import {NGXLogger} from 'ngx-logger';

export interface ErrorHandled {
  isOffLine: boolean;
  userFacingMsg: string;
  error: HttpErrorResponse;
}

@Injectable(  {providedIn: `root`})
export class UserService {

  private users = {
    nick: {
      name: 'Demo User',
      picture: 'assets/images/nick.png',
      email: 'demo@email.com',
    },
  };

  private userProfile = {};

  private usersUrl: string;

  private userArray: any[];

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private authService: NbAuthService,
    private errorHandlerService: CustomErrorHandlerService,
    private logger: NGXLogger,
  ) {
    this.userArray = Object.values(this.users);

  }

   getUserProfileObs(): Observable<any> {
    this.getUserProfile()
      .then((currentUser) => {
        Object.assign(this.userProfile, currentUser);
      });
    return observableOf(this.userProfile);
  }

  // get API user profile
  async getUserProfile() {
    const url = `${this.usersUrl}/profile`;
    const res = await this.httpClient.get<any>(url, { headers: this.getReqHeaders() })
      .toPromise()
      .catch(this.handleError);
    return res;
  }
  /**
   * PATCH User
   */
  async editUser(id: string, user: User): Promise<User> {
    try {
      const response = await this.httpClient
        .patch<User>(`${environment.apiUrl}/users/${id}`, user,
          { headers: this.getReqHeaders() })
        .toPromise();
      return response;
    } catch (e) {
      throw this.errorHandlerService.handleHttpError(e);
    }
  }

  /**
   * POST new User
   */
  async createUser(user: User): Promise<User> {
    try {
      const response = await this.httpClient
        .post<User>(`${environment.apiUrl}/users`, user,
          { headers: this.getReqHeaders() })
        .toPromise();
      return response;
    } catch (e) {
      throw this.errorHandlerService.handleHttpError(e);
    }
  }


  /**
   * GET User by id
   */
  async getUserById(id: string): Promise<User> {
    try {
      const response = await this.httpClient
        .get<User>(`${environment.apiUrl}/users/${id}`,
          { headers: this.getReqHeaders() })
        .toPromise();
      return response;
    } catch (e) {
      throw this.errorHandlerService.handleHttpError(e);
    }
  }

  /**
   * DELETE User by id
   */
  async deleteUserById(id: string): Promise<User> {
    try {
      const response = await this.httpClient
        .delete<User>(`${environment.apiUrl}/users/${id}`,
          { headers: this.getReqHeaders() })
        .toPromise();
      return response;
    } catch (e) {
      throw this.errorHandlerService.handleHttpError(e);
    }
  }

  /**
   * GET Users
   */
  async getUsers(): Promise<User[]> {
    try {
      const response = await this.httpClient
        .get<User[]>(`${environment.apiUrl}/users`,
          { headers: this.getReqHeaders() })
        .toPromise();
      return response;
    } catch (e) {
      throw this.errorHandlerService.handleHttpError(e);
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }

  getUserArray(): Observable<any[]> {
    return observableOf(this.userArray);
  }

  // getUser(): Observable<any> {
  //   counter = (counter + 1) % this.userArray.length;
  //   return observableOf(this.userArray[counter]);
  // }

  handleError(error: any) {
    const errMsg = error.message ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'ServerError';
    this.logger.log(errMsg);
  }

  getToken() {
    let user_token;
    this.authService.getToken()
      .subscribe(token => user_token = token);
    return user_token.token;
  }

  /**
   * Get http headers for httpClient request.
   * @return HttpHeaders with current API access_token .
   */
  getReqHeaders(): HttpHeaders {
    const token = this.getToken();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
    });
    return headers;
  }

}
