import {Injectable} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {Subject} from 'rxjs';

@Injectable(  {providedIn: `root`}
)
export class LocalDataService {

  statusOverlay$: Subject<boolean> = new Subject();

  constructor(private logger: NGXLogger) {
  }

  public store(farm): void {
    const existing = localStorage.getItem('currentFarm');
    // If existing or not data, always create/replace string object
    const data = existing ? JSON.stringify(farm) : JSON.stringify(farm);

    // Save back to localStorage
    localStorage.setItem('currentFarm', data);
  }


  public hasCurrentFarm(): boolean {
    const existing = localStorage.getItem('currentFarm');
    return existing !== null;
  }

  // user
  public storeUser(user: any): void {
    // debugger;
    this.logger.log('user to store: ', user.name);

    const existing = localStorage.getItem('currentUser');
    // If existing or not data, always create/replace string object
    const data = existing ? JSON.stringify(user) : JSON.stringify(user);

    // Save back to localStorage
    if (user.name) {
      localStorage.setItem('currentUser', data);
    } else {
      localStorage.removeItem('currentUser');
    }
  }

  // get local user
  public getCurrentUser(): any | null {
    const existing = localStorage.getItem('currentUser');
    let data: any | null = null;
    if (existing) {
      data = JSON.parse(existing);
    }
    return data;
  }

  public hasCurrentUser(): boolean {
    const existing = localStorage.getItem('currentUser');
    return existing !== null;
  }

  public clearStorageByFeedersList() {
    localStorage.removeItem('selectedPond');
    localStorage.removeItem('searchText');
  }

  public createStatesOfflineBehavior() {
    const existLastUpdateOffline = localStorage.getItem('lastUpdateOffline');
    const existIsEnabledAutoOffline = localStorage.getItem('isEnabledAutoOffline');
    if (!existLastUpdateOffline) {
      localStorage.setItem('lastUpdateOffline', null);
    }
    if (!existIsEnabledAutoOffline) {
      localStorage.setItem('isEnabledAutoOffline', JSON.stringify(false));
    }
  }

  public createStatesOverlayBehavior() {
    const existOverlay = localStorage.getItem('isEnabledOverlay');
    if (!existOverlay) {
      this.setEnabledOverlay(true);
      localStorage.setItem('overlayVersion',  JSON.stringify(0));
    }
  }

  public getEnabledAutoOffline() {
    return JSON.parse(localStorage.getItem('isEnabledAutoOffline'));
  }

  public getLastUpdateOffline() {
    return JSON.parse(localStorage.getItem('lastUpdateOffline'));
  }

  public setEnabledAutoOffline(state: boolean) {
    localStorage.setItem('isEnabledAutoOffline', JSON.stringify(state));
  }

  public setLastUpdateOffline(date: string) {
    localStorage.setItem('lastUpdateOffline', JSON.stringify(date));
  }

  public getEnabledOverlay() {
    return JSON.parse(localStorage.getItem('isEnabledOverlay'));
  }

  public getVersionOverlay() {
    return JSON.parse(localStorage.getItem('overlayVersion'));
  }

  public setEnabledOverlay(state: boolean) {
    localStorage.setItem('isEnabledOverlay', JSON.stringify(state));
    this.statusOverlay$.next(state);
    return this.statusOverlay$;
  }

}
