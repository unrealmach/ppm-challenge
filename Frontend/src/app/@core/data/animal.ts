export interface Animal {
  id?: any;
  name?: string;
  specie?: string;
  age?: number;
  gender?: string;
}
