import {Injectable, Input} from '@angular/core';
import {Subject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DevicesService {
  screenSize: Subject<any> = new Subject();
  isToMobile: Subject<any> = new Subject();
  @Input() detectSize: any;

  setSize(width: number): Observable<any> {
    if (this.detectSize === 'number') {
      this.screenSize.next(width);
    } else {
      if (width <= 425) {
        this.screenSize.next('small');
        this.isToMobile.next(true);
      } else {
        this.screenSize.next('large');
        this.isToMobile.next(false);
      }
    }
      return this.screenSize;
  }
}
