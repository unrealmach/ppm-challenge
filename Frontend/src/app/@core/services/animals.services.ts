import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Animal} from '../data/animal';
import {CustomErrorHandlerService} from '../utils/custom-error-handler.service';

@Injectable({
  providedIn: 'root',
})
export class AnimalsServices {
  private url_base = `${environment.apiUrl}`;
  private url_service = `${this.url_base}/animals`;

  constructor(private httpClient: HttpClient,
              private customErrorHandler: CustomErrorHandlerService) {
  }

  async getAnimals(): Promise<Animal[]> {
    try {
      const response = await this.httpClient
        .get<Animal[]>(this.url_service, {
          headers: new HttpHeaders({
            'Access-Control-Allow-Origin': '*',
          }),
        })
        .toPromise();
      return response;
    } catch (e) {
      throw this.customErrorHandler.handleHttpError(e);
    }
  }

  async deleteAnimal(id: number): Promise<void> {
    try {
      await this.httpClient.delete<void>(`${this.url_service}/${id}`)
        .toPromise();
    } catch (e) {
      throw this.customErrorHandler.handleHttpError(e);
    }
  }

  async getAnimal(animalId: any) {
    try {
      const response = await this.httpClient
        .get<Animal>(`${this.url_service}/${animalId}`)
        .toPromise();
      return response;
    } catch (e) {
      throw this.customErrorHandler.handleHttpError(e);
    }
  }

  async editAnimal(animalId: any, animal: Animal) {
    try {
      const response = await this.httpClient
        .put<Animal>(`${this.url_service}/${animalId}`, animal)
        .toPromise();
      return response;
    } catch (e) {
      throw this.customErrorHandler.handleHttpError(e);
    }
  }

  async patchAnimal(animalId: any, animal: Animal) {
    try {
      const response = await this.httpClient
        .put<Animal>(`${this.url_service}/${animalId}`, animal)
        .toPromise();
      return response;
    } catch (e) {
      throw this.customErrorHandler.handleHttpError(e);
    }
  }

  async newAnimal(animal: Animal) {
    try {
      const response = await this.httpClient
        .post<Animal>(`${this.url_service}`, animal)
        .toPromise();
      return response;
    } catch (e) {
      throw this.customErrorHandler.handleHttpError(e);
    }
  }
}
