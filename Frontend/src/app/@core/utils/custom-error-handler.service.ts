import {ErrorHandler, Injectable} from '@angular/core';
import {NbToastrService} from '@nebular/theme';
import {HttpErrorResponse} from '@angular/common/http';
import {NGXLogger} from 'ngx-logger';
import {ErrorHandled} from '../data/users.service';

@Injectable(
  {providedIn: 'root'})

export class CustomErrorHandlerService implements ErrorHandler {
  constructor(private toastrService: NbToastrService,
              private logger: NGXLogger,
  ) {
  }

  handleError(error) {
    console.error(error);
  }

  /**
   * error handler by angular.io HttpClient with Observables.
   * @param  error error occurred in http request
   * @return       ErrorEvent via throwError.
   */
  handleHttpError(error: HttpErrorResponse): ErrorHandled {
    let userFacingMsg: string = '';
    let isOffLine: boolean = false;

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
      userFacingMsg = 'Ocurrio un error con su dispositivo';
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);

      if (!navigator.onLine) {
        this.logger.log('OFFLINE');
        userFacingMsg = 'Sin conexion.!';
        isOffLine = true;
      } else {
        this.logger.log('ONLINE', navigator);
        userFacingMsg = 'Algo salio mal; por favor intente en un momento.';
      }
    }
    // return an with a user-facing ErrorHandled
    return {
      isOffLine,
      userFacingMsg,
      error,
    };
  }

  handleIdbError(reason) {
    console.error(reason);
  }
}
