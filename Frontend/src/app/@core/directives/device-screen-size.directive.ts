import {Directive, HostListener} from '@angular/core';
import {DevicesService} from '../services/devices.services';

@Directive({
  selector: `[ngxDeviceScreen]`,
})
export class DeviceScreenSizeDirective {
  constructor(
    private devices: DevicesService,
  ) {
  }

  @HostListener('window:load', ['$event']) onLoad(event) {
    this.sendSize(event.currentTarget.innerWidth);
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.sendSize(event.currentTarget.innerWidth);
  }

  sendSize(width: number) {
    this.devices.setSize(width);
  }
}
