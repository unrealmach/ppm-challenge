import {CustomErrorHandlerService} from './@core/utils/custom-error-handler.service';
import {APP_BASE_HREF} from '@angular/common';
import {ErrorHandler, NgModule} from '@angular/core';
import {AuthGuard} from './@core/data/auth-guard.service';
import {AppRoutingModule} from './app-routing.module';
import {NbDatepickerModule, NbDialogModule, NbMenuModule, NbToastrModule, NbWindowModule} from '@nebular/theme';
import {NbAuthJWTToken, NbAuthModule, NbAuthService, NbPasswordAuthStrategy} from '@nebular/auth';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {ThemeModule} from './@theme/theme.module';
import {AppComponent} from './app.component';
import {CoreModule} from './@core/core.module';
import {HttpClientModule} from '@angular/common/http';
import {NbEvaIconsModule} from '@nebular/eva-icons';
import {environment} from '../environments/environment';
import {LoggerModule} from 'ngx-logger';

@NgModule({
  declarations: [AppComponent],
  imports: [
    NbEvaIconsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbDialogModule.forRoot(),
    NbMenuModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    NbWindowModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    // auth configs
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          token: {
            class: NbAuthJWTToken,
            key: 'token.accessToken',
          },
          baseEndpoint: `${environment.apiUrl}/auth/`,
          login: {
            endpoint: 'login',
            method: 'post',
          },
          register: {
            endpoint: 'register',
            method: 'post',
          },
          logout: {
            endpoint: 'sign-out',
            method: 'post',
          },
        }),
      ],
      forms: {
        login: {
          // delay before redirect after a successful login, while success message is shown to the user
          redirectDelay: 0,
          strategy: 'email',  // strategy id key.
          rememberMe: true,   // whether to show or not the `rememberMe` checkbox
          showMessages: {     // show/not show success/error messages
            success: true,
            error: true,
          },
        },
        validation: {
          password: {
            required: true,
            minLength: 6,
            maxLength: 128,
          },
          email: {
            required: true,
          },
          fullName: {
            required: false,
            minLength: 4,
            maxLength: 50,
          },
        },
      },
    }),
    LoggerModule.forRoot({
      // serverLoggingUrl: '/api/logs',
      level: environment.ngxLoggerLvl,
      // serverLogLevel: NgxLoggerLevel.DEBUG,
      timestampFormat: 'short',
    }),
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'},
    // auth services
    NbAuthService,
    AuthGuard,
    {provide: ErrorHandler, useClass: CustomErrorHandlerService},
  ],
})
export class AppModule {
}
