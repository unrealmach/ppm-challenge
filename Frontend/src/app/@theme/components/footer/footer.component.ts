import {Component, OnDestroy} from '@angular/core';
// @ts-ignore
import {version} from '../../../../../package.json';
import {DevicesService} from '../../../@core/services/devices.services';
import {distinctUntilChanged} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  templateUrl: './footer.component.html',
})
export class FooterComponent implements OnDestroy {
  public version: string = version;
  public year = (new Date()).getFullYear();

  isDesktopDataTable: boolean = true;
  size$: Subscription;

  constructor(private device: DevicesService,
  ) {
    this.checkSize();
  }

  private checkSize() {
    this.size$ = this.device.isToMobile.pipe(distinctUntilChanged())
      .subscribe(value => this.isDesktopDataTable = !value);
    this.device.setSize(window.innerWidth);
  }

  ngOnDestroy() {
    this.size$.unsubscribe();
  }

}
