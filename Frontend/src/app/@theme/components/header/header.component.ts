import {Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {
  NbMenuService,
  NbSidebarService,
  NbMenuItem,
  NbToastrService,
  NbDialogService,
  NbThemeService,
  NbMediaBreakpointsService,
} from '@nebular/theme';
import {UserService} from '../../../@core/data/users.service';
import {AnalyticsService} from '../../../@core/utils/analytics.service';
import {LayoutService} from '../../../@core/data/layout.service';
import {LocalDataService} from '../../../@core/data/local-data.service';
import {CustomErrorHandlerService} from '../../../@core/utils/custom-error-handler.service';
import {Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {NGXLogger} from 'ngx-logger';

interface ContextMenuItem {
  title: string;
  icon: string;
  farmId: string;
  tag: string;
}

class Farm {
}

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject<void>();
  themes = [
    {
      value: 'dark',
      name: 'Oscuro',
    },
    {
      value: 'corporate',
      name: 'Light',
    },
  ];
  currentTheme = 'corporate';
  userPictureOnly: boolean = false;

  @Input() position = 'normal';

  public user;
  public userProfile;
  public farms: Farm[] | void;
  public currentFarm: Farm;
  public companyFarmNames: string;

  farmsContextMenu: ContextMenuItem[];

  userMenu: NbMenuItem[] = [
    {title: 'USUARIO', group: true},
    {title: 'Mi Perfil', icon: 'person-done-outline'},
    {title: 'Log out', icon: 'power-outline'},
  ];

  onlineMode = true;


  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserService,
    private localDataService: LocalDataService,
    private analyticsService: AnalyticsService,
    private layoutService: LayoutService,
    private router: Router,
    private toastService: NbToastrService,
    private errorHandlerService: CustomErrorHandlerService,
    private themeService: NbThemeService,
    private breakpointService: NbMediaBreakpointsService,
    private logger: NGXLogger,
  ) {
  }


  async ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;
    const {xl} = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({name}) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

   toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

}
