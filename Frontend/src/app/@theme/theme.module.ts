import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  NbActionsModule,
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSidebarModule,
  NbUserModule,
  NbContextMenuModule,
  NbButtonModule,
  NbSelectModule,
  NbThemeModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbTabsetModule,
  NbCheckboxModule,
  NbPopoverModule,
  NbProgressBarModule,
  NbCalendarModule,
  NbCalendarRangeModule,
  NbStepperModule,
  NbInputModule,
  NbAccordionModule,
  NbDatepickerModule,
  NbDialogModule,
  NbListModule,
  NbWindowModule,
  NbToastrModule,
  NbAlertModule,
  NbSpinnerModule,
  NbRadioModule,
  NbTooltipModule,
  NbCalendarKitModule,
  NbBadgeModule, NbIconModule, NbToggleModule,
} from '@nebular/theme';

import {
  FooterComponent,
  ThemeSettingsComponent,
  HeaderComponent,
  SwitcherComponent,
  LayoutDirectionSwitcherComponent,
} from './components';
import {
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
} from './pipes';
import {SampleLayoutComponent} from './layouts';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EvaIconsPipe} from './pipes/eva-icons.pipe';
import {NbSecurityModule} from '@nebular/security';
import {ToggleSettingsButtonComponent} from './components/toggle-settings-button/toggle-settings-button.component';
import {DARK_THEME} from './styles/theme.dark';
import {CORPORATE_THEME} from './styles/theme.corporate';
import {DeviceScreenSizeDirective} from '../@core/directives/device-screen-size.directive';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule];

const NB_MODULES = [
  NbActionsModule,
  NbCardModule,
  NbLayoutModule,
  NbTabsetModule,
  NbMenuModule,
  NbRouteTabsetModule,
  NbSearchModule,
  NbUserModule,
  NbActionsModule,
  NbSidebarModule,
  NbTabsetModule,
  NbThemeModule,
  NbUserModule,
  NbCheckboxModule,
  NbPopoverModule,
  NbContextMenuModule,
  NbProgressBarModule,
  NbCalendarModule,
  NbCalendarRangeModule,
  NbStepperModule,
  NbButtonModule,
  NbListModule,
  NbInputModule,
  NbAccordionModule,
  NbDatepickerModule,
  NbDialogModule,
  NbWindowModule,
  NbListModule,
  NbToastrModule,
  NbAlertModule,
  NbSpinnerModule,
  NbRadioModule,
  NbSelectModule,
  NbTooltipModule,
  NbCalendarKitModule,
  NbBadgeModule,
  NbToastrModule,
  NbSecurityModule,
  NbBadgeModule,
];


const COMPONENTS = [
  SwitcherComponent,
  LayoutDirectionSwitcherComponent,
  HeaderComponent,
  FooterComponent,
  ThemeSettingsComponent,
  SampleLayoutComponent,
  ToggleSettingsButtonComponent,
  DeviceScreenSizeDirective,
];

const ENTRY_COMPONENTS = [];

const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  EvaIconsPipe,
];


const NB_THEME_PROVIDERS = [
  ...NbThemeModule.forRoot(
    {
      name: 'corporate',
      // name: 'dark',
    },
    [CORPORATE_THEME, DARK_THEME],
  ).providers,
  ...NbSidebarModule.forRoot().providers,
  ...NbMenuModule.forRoot().providers,
  ...NbDatepickerModule.forRoot().providers,
  ...NbDialogModule.forRoot().providers,
  ...NbWindowModule.forRoot().providers,
  ...NbToastrModule.forRoot().providers,
];

@NgModule({
  imports: [...BASE_MODULES, ...NB_MODULES, NbIconModule, NbToggleModule],
  exports: [...BASE_MODULES, ...NB_MODULES, ...COMPONENTS, ...PIPES],
  declarations: [...COMPONENTS, ...PIPES, ToggleSettingsButtonComponent],
  entryComponents: [...ENTRY_COMPONENTS],
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders<ThemeModule> {
    return {
      ngModule: ThemeModule,
      providers: [...NB_THEME_PROVIDERS],
    };
  }
}
