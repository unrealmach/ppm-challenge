/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {Component, OnInit} from '@angular/core';
import {AnalyticsService} from './@core/utils/analytics.service';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet ngxDeviceScreen></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(private analytics: AnalyticsService,
              //      , private seoService: SeoService
  ) {
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
    //  this.seoService.trackCanonicalChanges();
  }
}
