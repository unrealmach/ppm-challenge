import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'bar-chart',
    home: true,
    link: '/pages/animal/index',
  },
  {
    title: 'SISTEMA',
    group: true,
  }];
