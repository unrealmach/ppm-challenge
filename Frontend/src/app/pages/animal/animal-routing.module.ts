import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AnimalComponent} from './animal.component';
import {AnimalIndexComponent} from './animal-index/animal-index.component';
import {AnimalCreateComponent} from './animal-create/animal-create.component';
import {AnimalUpdateComponent} from './animal-update/animal-update.component';

const routes: Routes = [
  {
    path: '',
    component: AnimalComponent,
    children: [
      {path: 'index', component: AnimalIndexComponent},
      {path: '', redirectTo: 'index', pathMatch: 'full'},
    ],
  },
  {
    path: 'create',
    component: AnimalCreateComponent,
  },
  {
    path: 'update/:id',
    component: AnimalUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  entryComponents: [],
})

export class AnimalRoutingModule {
}

export const routedComponents = [
  AnimalComponent,
  AnimalIndexComponent,
  AnimalCreateComponent,
  AnimalUpdateComponent,
];
