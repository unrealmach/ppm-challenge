// angulars
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// dependencies
import { ThemeModule } from '../../@theme/theme.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { AnimalRoutingModule, routedComponents } from './animal-routing.module';
import {NbIconModule} from '@nebular/theme';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NgxEchartsModule,
    AnimalRoutingModule,
    NbIconModule,
    NgxDatatableModule,
  ],
  providers: [
    // services
  ],
  declarations: [
    ...routedComponents,
  ],
})

export class AnimalModule { }
