import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AnimalsServices} from '../../../@core/services/animals.services';
import {NbToastrService} from '@nebular/theme';
import {ActivatedRoute} from '@angular/router';
import {Animal} from '../../../@core/data/animal';
import {ErrorHandled} from '../../../@core/data/users.service';

@Component({
  selector: 'ngx-animal-update',
  templateUrl: './animal-update.component.html',
  styleUrls: ['./animal-update.component.scss'],
})
export class AnimalUpdateComponent implements OnInit {
  editAnimalForm: FormGroup;
  isLoadingData: boolean = false;
  loading: boolean = false;
  animalId: any;
  animal: Animal;

  constructor(private formBuilder: FormBuilder,
              private animalService: AnimalsServices,
              private toastrService: NbToastrService,
              private route: ActivatedRoute,
  ) {
  }


  get name() {
    return this.editAnimalForm.get('name');
  }

  get age() {
    return this.editAnimalForm.get('age');
  }

  get gender() {
    return this.editAnimalForm.get('gender');
  }

  get specie() {
    return this.editAnimalForm.get('specie');
  }

  async ngOnInit() {
    this.animalId = this.route.snapshot.paramMap.get('id');
    try {
      await this.createForm();
      this.editAnimalForm.get('name').valueChanges.subscribe(name => this.animal['name'] = name);
      this.editAnimalForm.get('specie').valueChanges.subscribe(specie => this.animal['specie'] = specie);
      this.editAnimalForm.get('gender').valueChanges.subscribe(gender => this.animal['gender'] = gender);
      this.editAnimalForm.get('age').valueChanges.subscribe(age => this.animal['age'] = age);
    } catch (eHandled) {
      this.handleError(eHandled);
    }
  }

  async createForm() {
    this.animal = await this.animalService.getAnimal(this.animalId);

    this.editAnimalForm = await this.formBuilder.group({
      name: [this.animal.name, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
      ]],
      specie: [this.animal.specie, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
      ]],
      age: [this.animal.age, [
        Validators.required,
        Validators.minLength(2),
      ]],
      gender: [this.animal.gender, [
        Validators.required,
        Validators.minLength(2),
      ]],
    });
  }

  editAnimal() {
    this.loading = true;

    this.animalService.editAnimal(this.animalId, this.animal)
      .then(response => {
        this.loading = false;
        this.toastrService.success('EXITO', 'EDITADO');
        setTimeout(() => window.location.replace('/'), 2000);
      }).catch(() => {
      this.toastrService.danger('ERROR', 'NO EDITADO');
      this.loading = true;
    });

  }


  goBack() {
  }

  handleError(eHandled: ErrorHandled) {
    this.loading = false;
    this.toastrService.danger(eHandled, 'Error');
    this.goBack();
  }
}
