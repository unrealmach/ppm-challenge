import {Component, OnInit} from '@angular/core';
import {AnimalsServices} from '../../../@core/services/animals.services';
import {ColumnMode, SelectionType} from '@swimlane/ngx-datatable';
import {Animal} from '../../../@core/data/animal';
import {NGXLogger} from 'ngx-logger';
import {ErrorHandled} from '../../../@core/data/users.service';
import {NbToastrService} from '@nebular/theme';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-feedplan-index',
  templateUrl: './animal-index.component.html',
  styleUrls: ['./animal-index.component.scss'],
})
export class AnimalIndexComponent implements OnInit {
  loading = true;
  paginationSelected: number;
  ColumnMode: any = ColumnMode;
  SelectionType: any = SelectionType;
  rows: any[] = [];
  rowsTemporal: any[] = [];
  rowsTemp: Animal[] = [];

  constructor(
    private  animalsServices: AnimalsServices,
    private logger: NGXLogger,
    private toastrService: NbToastrService,
    private router: Router,
  ) {
  }

  async ngOnInit() {
    this.paginationSelected = 25;
    await this.feedInformation();
  }

  changePaginationSelected(value: any) {
    this.paginationSelected = value;
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const columns = [
      {name: 'name'},
    ];
    // filter our data
    if (val.length === 0) {
      this.rows = this.rowsTemp;
    } else {
      const temp = [];
      this.rowsTemporal.filter(function (animal) {
        columns.forEach((column) => {
          if ((animal[column.name].toString().toLowerCase().indexOf(val) !== -1 || !val)) {
            if (!(animal in temp)) {
              temp.push(animal);
            }
          }
        });
      });
      // update the rows
      if (temp.length !== 0) this.rows = Array.from(new Set(temp));
    }
  }

  async fillDataTable() {
    this.loading = true;
    const animals = await this.animalsServices.getAnimals();
    this.rows = animals
      .sort((a: any, b: any) => a.name.localeCompare(b.name, undefined, {numeric: true}));
    this.rowsTemporal = [...this.rows];
    this.rowsTemp = [...this.rows];
    this.loading = false;
  }

  handleError(eHandled: ErrorHandled) {
    this.loading = false;
    this.toastrService.danger(eHandled, 'Error');
    this.goBack();
  }

  goBack() {
    this.router.navigate(['/pages/']);
  }

  goToCreate() {
    this.router.navigate(['/pages/animal/create']);
  }

  async delete(id: number) {
    try {
      await this.animalsServices.deleteAnimal(id);
      await this.feedInformation();
    } catch (e) {
      this.handleError(e);
    }
  }

  update(id: any) {
    this.router.navigate([`/pages/animal/update/${id}`]);
  }

  async setGender(id: any, gender: any) {
    const newGenere = gender === 'male' ? {gender: 'female'} : {gender: 'male'};

    try {
      await this.animalsServices.patchAnimal(id, newGenere);
      await this.feedInformation();
    } catch (e) {
      this.handleError(e);
    }
  }

  private async feedInformation() {
    try {
      await this.fillDataTable();

    } catch (eHandled) {
      this.logger.log(eHandled);
      this.handleError(eHandled);
    }
  }
}
