import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Animal} from '../../../@core/data/animal';
import {AnimalsServices} from '../../../@core/services/animals.services';
import {NbToastrService} from '@nebular/theme';
import {ActivatedRoute} from '@angular/router';
import {ErrorHandled} from '../../../@core/data/users.service';

@Component({
  selector: 'ngx-animal-create',
  templateUrl: './animal-create.component.html',
  styleUrls: ['./animal-create.component.scss'],
})
export class AnimalCreateComponent implements OnInit {

  newAnimalForm: FormGroup;
  isLoadingData: boolean = false;
  loading: boolean = false;
  animalId: any;
  animal: Animal;

  constructor(private formBuilder: FormBuilder,
              private animalService: AnimalsServices,
              private toastrService: NbToastrService,
              private route: ActivatedRoute,
  ) {
    this.animal = {specie: '', name: '', age: 1, gender: 'male'};
  }


  get name() {
    return this.newAnimalForm.get('name');
  }

  get age() {
    return this.newAnimalForm.get('age');
  }

  get gender() {
    return this.newAnimalForm.get('gender');
  }

  get specie() {
    return this.newAnimalForm.get('specie');
  }

  async ngOnInit() {
    try {
      await this.createForm();
      this.newAnimalForm.get('name').valueChanges.subscribe(name => this.animal['name'] = name);
      this.newAnimalForm.get('specie').valueChanges.subscribe(specie => this.animal['specie'] = specie);
      this.newAnimalForm.get('gender').valueChanges.subscribe(gender => this.animal['gender'] = gender);
      this.newAnimalForm.get('age').valueChanges.subscribe(age => this.animal['age'] = age);
    } catch (eHandled) {
      this.handleError(eHandled);
    }
  }

  async createForm() {
    this.newAnimalForm = await this.formBuilder.group({
      name: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
      ]],
      specie: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
      ]],
      age: [1, [
        Validators.required,
        Validators.minLength(2),
      ]],
      gender: ['male', [
        Validators.required,
        Validators.minLength(2),
      ]],
    });
  }

  newAnimal() {
    this.loading = true;

    this.animalService.newAnimal(this.animal)
      .then(response => {
        this.loading = false;
        this.toastrService.success('EXITO', 'GUARDADO');
        setTimeout(() => window.location.replace('/'), 2000);
      }).catch(() => {
      this.toastrService.danger('ERROR', 'NO GUARDADO');
      this.loading = true;
    });

  }


  goBack() {
  }

  handleError(eHandled: ErrorHandled) {
    this.loading = false;
    this.toastrService.danger(eHandled, 'Error');
    this.goBack();
  }
}
