import { NgModule } from '@angular/core';
import {ThemeModule} from '../@theme/theme.module';
import {PagesRoutingModule} from './pages-routing.module';
import {PagesComponent} from './pages.component';
import {MiscellaneousModule} from './miscellaneous/miscellaneous.module';
import {NbMenuModule} from '@nebular/theme';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    MiscellaneousModule,
    NbMenuModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
})
export class PagesModule {
}
