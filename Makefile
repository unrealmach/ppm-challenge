
default:
	@echo "The following commands are available:"
	@echo ""
	@echo "  make start        start PHP built-in webserver"
	@echo "  make stop         stop PHP built-in webserver"

# start PHP built-in webserver
start:
	@echo "Starting server for api"
	cd api && $(MAKE) start
	@echo "Starting server for backend"
	cd backend && $(MAKE) start

# stop PHP built-in webserver
stop:
	cd api && $(MAKE) stop
	cd backend && $(MAKE) stop

test:
	cd api && $(MAKE) test

clean: stop

.PHONY: start stop clean test

docker-up: config/components-dev.local.php config/components-test.local.php env.php stop
	docker-compose up -d
	docker-compose exec backend-php sh -c 'cd /app && composer install'
	docker-compose exec backend-php sh -c 'cd /app && ./yii migrate --interactive=0'
	@echo ""
	@echo "API:      http://localhost:1111/"
	@echo "Backend:  http://localhost:1112/"
	@echo "Fronted:  http://localhost:1113/"
	@echo ""

cli:
	docker-compose exec backend-php bash

cli-db:
	docker-compose exec db bash

cli-angular:
	docker-compose exec angular10 sh
# copy config files if they do not exist
config/components-%.local.php: config/components-ENV.local.php
	test -f $@ || cp $< $@
env.php: env.php.dist
	test -f $@ || cp $< $@
