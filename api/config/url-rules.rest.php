<?php
/**
 * OpenAPI UrlRules
 *
 * This file is auto generated.
 */
return array (
  'GET api/animals' => 'animal/index',
  'POST api/animals' => 'animal/create',
  'GET api/animals/<id>' => 'animal/view',
  'PUT api/animals/<id>' => 'animal/update',
  'DELETE api/animals/<id>' => 'animal/delete',
  'PATCH api/animals/<id>' => 'animal/update',
);
