<?php

/**
 * Table for Error
 */
class m201103_000000_Error extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('{{%errors}}', [
            'code' => 'integer',
            'message' => 'text',
        ]);

        // TODO generate foreign keys
    }

    public function down()
    {
        $this->dropTable('{{%errors}}');
    }
}
