<?php

/**
 * Table for Animal
 */
class m201103_000000_Animal extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('{{%animals}}', [
            'id' => 'pk',
            'name' => 'text',
            'specie' => 'text',
            'age' => 'integer',
            'gender' => 'text',
        ]);

        // TODO generate foreign keys
    }

    public function down()
    {
        $this->dropTable('{{%animals}}');
    }
}
