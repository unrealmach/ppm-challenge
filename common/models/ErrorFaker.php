<?php

namespace common\models;

use Faker\Factory as FakerFactory;

/**
 * Fake data generator for Error
 */
class ErrorFaker
{
    public function generateModel()
    {
        $faker = FakerFactory::create(\Yii::$app->language);
        $model = new Error;
        $model->code = $faker->numberBetween(-1, PHP_INT_MAX);
        $model->message = $faker->sentence;
        return $model;
    }
}
