<?php

namespace common\models;

/**
 * 
 *
 * @property int $code
 * @property string $message
 */
class Error extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%errors}}';
    }

    public function rules()
    {
        return [
            [['message'], 'trim'],
            [['code', 'message'], 'required'],
            [['message'], 'string'],
            // TODO define more concreate validation rules!
            [['code'], 'safe'],
        ];
    }

}
