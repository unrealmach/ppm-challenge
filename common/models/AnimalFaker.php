<?php

namespace common\models;

use Faker\Factory as FakerFactory;

/**
 * Fake data generator for Animal
 */
class AnimalFaker
{
    public function generateModel()
    {
        $faker = FakerFactory::create(\Yii::$app->language);
        $model = new Animal;
        $model->id = $faker->numberBetween(-1, PHP_INT_MAX);
        $model->name = $faker->sentence;
        $model->specie = $faker->sentence;
        $model->age = $faker->numberBetween(-1, PHP_INT_MAX);
        $model->gender = $faker->sentence;
        return $model;
    }
}
