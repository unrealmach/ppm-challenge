<?php

namespace common\models;

/**
 * 
 *
 * @property int $id
 * @property string $name
 * @property string $specie
 * @property int $age
 * @property string $gender
 */
class Animal extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%animals}}';
    }

    public function rules()
    {
        return [
            [['name', 'specie', 'gender'], 'trim'],
            [['name', 'specie', 'age', 'gender'], 'required'],
            [['name', 'specie', 'gender'], 'string'],
            // TODO define more concreate validation rules!
            [['id','age'], 'safe'],
        ];
    }

}
